from copy import deepcopy
import unittest
import json

import sys
sys.path.append('../database')
from services.database import Database
data = Database()
import app


#BAD_ITEM_URL = '{}/5'.format(BASE_URL)
#GOOD_ITEM_URL = '{}/1'.format(BASE_URL)


class TestFlaskApi(unittest.TestCase):

    def setUp(self):
        self.backup_items = deepcopy(data.storage)  # no references!
        self.app = app.app.test_client()
        self.app.testing = True

    def test_get_allUsers(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_get_oneUser(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data[0]['self'], 'rest/user/1')
        self.assertEqual(data[0]['id'], 1)
        self.assertEqual(data[0]['alias'], 'pullman')
        self.assertEqual(data[0]['name'], 'Alex')
        self.assertEqual(data[0]['surname'], 'Martin')
        self.assertEqual(data[0]['age'], 25)
        self.assertEqual(data[0]['phone'], 680159369)
        self.assertEqual(data[0]['grupo'], {u'href': u'rest/grupo/grupo1', u'name': u'grupo1'})
        self.assertEqual(data[0]['photo'], '123')

    def test_get_one_UserNotExist(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/4'
        response = self.app.get(BASE_URL)
        self.assertEqual(response.status_code, 404)

    def test_get_one_UserEspecifiDataSefl(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/self'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'self': u'rest/user/1'})

    def test_get_one_UserEspecifiDataAlias(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/alias'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'alias': u'pullman', u'self': u'rest/user/1/alias'})

    def test_get_one_UserEspecifiDataId(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/id'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'id': 1, u'self': u'rest/user/1/id'})

    def test_get_one_UserEspecifiDataName(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/name'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'name': u'Alex', u'self': u'rest/user/1/name'})

    def test_get_one_UserEspecifiDataSurName(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/surname'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'surname': u'Martin', u'self': u'rest/user/1/surname'})

    def test_get_one_UserEspecifiDataAge(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/age'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'age': 25, u'self': u'rest/user/1/age'})

    def test_get_one_UserEspecifiDataPhone(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/phone'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'phone': 680159369, u'self': u'rest/user/1/phone'})

    def test_get_one_UserEspecifiDataGrupo(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/grupo'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'grupo': {u'href': u'rest/grupo/grupo1', u'name': u'grupo1'}, u'self': u'rest/user/1/grupo'})

    def test_get_one_UserEspecifiDataPhoto(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/photo'
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, {u'photo': u'123', u'self': u'rest/user/1/photo'})

    def test_get_one_UserEspecifiDataNoUser(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/3/name'
        response = self.app.get(BASE_URL)
        self.assertEqual(response.status_code, 404)

    def test_get_one_UserEspecifiDataFalseData(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/3/dni'
        response = self.app.get(BASE_URL)
        self.assertEqual(response.status_code, 404)


    def test_post(self):
        # missing alias and surname
        BASE_URL = 'http://127.0.0.1:5000/rest/user'
        item = {"name": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        # missing name and surname
        BASE_URL = 'http://127.0.0.1:5000/rest/user'
        item = {"surname": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        # missing alias and name
        BASE_URL = 'http://127.0.0.1:5000/rest/user'
        item = {"surname": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        # missing alias, surname and name
        BASE_URL = 'http://127.0.0.1:5000/rest/user'
        item = {"age": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)
        # ok post
        item = {"name": "Moises", "surname": "Ramos", "alias": "Moi", "age": 27}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.get_data())
        self.assertEqual(data, {u'surname': u'Ramos', u'name': u'Moises', u'self': u'rest/user/3', u'id': 3, u'alias': u'Moi', u'age': 27})
        # post same alias
        item = {"name": "Moises", "surname": "Ramirez", "alias": "Moi", "id": 3, "age": 27, "self": "rest/user/3"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(item),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_update_UserData(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/alias'
        item = {"alias": "cuchi"}
        response = self.app.put(BASE_URL,
                                data=json.dumps(item),
                                content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data())
        self.assertEqual(data[0]['alias'], "cuchi")

    def test_delete_UserData(self):
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/alias'
        response = self.app.delete(BASE_URL)
        self.assertEqual(response.status_code, 201)
        data = json.loads(response.get_data())
        self.assertEqual(data, {u'Succes Completed': u'Element Deleted'})

        URLTEST = 'http://127.0.0.1:5000/rest/user/1'
        response = self.app.get(URLTEST)
        data = json.loads(response.get_data())
        print data
        self.assertEqual(response.status_code, 404)

    def test_update_error(self):
        # cannot edit non-existing item
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/dni'
        item = {"dni": "Willy"}
        response = self.app.put(BASE_URL,
                                data=json.dumps(item),
                                content_type='application/json')
        self.assertEqual(response.status_code, 404)
        # value field cannot take str
        BASE_URL = 'http://127.0.0.1:5000/rest/user/1/surname'
        item = {"surname": 25}
        response = self.app.put(BASE_URL,
                                data=json.dumps(item),
                                content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_delete(self):
    	GOOD_ITEM_URL = 'http://127.0.0.1:5000/rest/user/1/surname'
        response = self.app.delete(GOOD_ITEM_URL)
        self.assertEqual(response.status_code, 204)
        BAD_ITEM_URL = 'http://127.0.0.1:5000/rest/user/1/dni'
        response = self.app.delete(BAD_ITEM_URL)
        self.assertEqual(response.status_code, 404)

    def tearDown(self):
        # reset app.items to initial state
        app.items = self.backup_items


if __name__ == "__main__":
    unittest.main()