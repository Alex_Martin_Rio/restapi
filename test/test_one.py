import os 

tests = """
# get users
curl http://localhost:5000/rest/user
# user 1
curl http://localhost:5000/rest/user/1
# user 2
curl http://localhost:5000/rest/user/2
# err: non-existing item
curl http://localhost:5000/rest/user/4
# err: add info already in user
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/user --data "{"""alias""":"""pullman""","""name""":"""Alex""","""surname""":"""Martin"""}"
# err: add user with name int 
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/user --data "{"""alias""":"""Moi""","""name""":"""25""","""surname""":"""Ramos"""}"
# add item with proper values
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/user --data "{"""alias""":"""Moi""","""name""":"""Moises""","""surname""":"""Ramos"""}"
# show items
curl http://localhost:5000/rest/user
# err: edit non-existing info
curl --header "Content-Type: application/json" --request PUT http://localhost:5000/rest/user/5/alias --data "{"""alias""":"""pull"""}"
# OK: edit existing info
curl --header "Content-Type: application/json" --request PUT http://localhost:5000/rest/user/1/alias --data "{"""alias""":"""pull"""}"
# show items
curl http://localhost:5000/rest/user
# err: delete non-existing info
curl http://localhost:5000/rest/user/5/alias
# OK: delete existing item
curl http://localhost:5000/rest/user/1/alias
# show items
curl http://localhost:5000/rest/user
# get groups
curl http://localhost:5000/rest/grupo
# err: non-existing item
curl http://localhost:5000/rest/grupo/4
# err: add info already in group
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/grupo --data "{"""name""":"""admin"""}"
# err: add group with name int 
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/grupo --data "{"""name""":"""1"""}"
# add item with proper values
curl --header "Content-Type: application/json" --request POST http://localhost:5000/rest/grupo --data "{"""name""":"""center"""}"
# show items
curl http://localhost:5000/rest/grupo
"""

for line in tests.strip().split('\n'):
    print('\n{}'.format(line))
    if not line.startswith('#'):
        cmd = line.strip() 
        os.system(cmd)