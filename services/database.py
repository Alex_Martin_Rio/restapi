from flask import jsonify, abort, make_response
import json

class Database:

    def __init__(self):
        self.storage = [
            {
                'self': 'rest/user/1',
                'id': 1,
                'alias': 'pullman',
                'name': 'Alex',
                'surname': 'Martin',
                'age': 25,
                'phone': 680159369,
                'grupo':
                    {
                        'name': 'Administrador',
                        'href': 'rest/grupo/administrador'
                    },
                'photo': '123'
            },
            {
                'self': 'rest/user/2',
                'id': 2,
                'alias': 'cuchi',
                'name': 'Albert',
                'surname': 'Cucharero',
                'age': 27,
                'phone': 632598147,
                'grupo':
                    {
                        'name': 'Punter',
                        'href': 'rest/grupo/punter'
                    },
                'photo': '456'
            }
        ]

        self.group_storage = [
            {
                'self': 'rest/group/administrador',
                'name': 'Administrador'
            },
            {
                'self': 'rest/group/punter',
                'name': 'Punter'
            }
        ]

    def get_users(self):
        #return make_response(jsonify({'error': 'Not found'}), 201)
        return jsonify(self.storage)

    def get_user_ById(self, id):
        u = [user for user in self.storage if user['id'] == id]
        if len(u) == 0:
            abort(404)
        return jsonify(u)

    def get_User_DataByID(self, id, data):
        u = [user for user in self.storage if user['id'] == id]
        if len(u) == 0:
            abort(404)
        return jsonify({"self": u[0]['self']+"/"+str(data), str(data): u[0][data]})

    def updatae_user_DataById(self, id, data, json_object):
        u = [user for user in self.storage if user['id'] == id]
        if len(u) == 0:
            abort(404)
        u[0][data] = json_object.get(data, u[0][data])
        return jsonify(u)

    def delete_user_DatabyID(self, id, data):
        u = [user for user in self.storage if user['id'] == id]
        print type(u)
        if len(u) == 0:
            abort(404)
        del u[0][data]
        return make_response(jsonify({'Succes Completed': 'Element Deleted'}), 201)

    def add_user(self, json_object):
        id = self.storage[-1]['id'] + 1
        u = [user for user in self.storage if user['alias'] == json_object['alias']]
        if len(u) != 0:
            abort(400)
        new_user = {
            'self': "rest/user/" + str(id),
            'id': id,
            'alias': json_object['alias'],
            'name': json_object['name'],
            'surname': json_object['surname'],
            'age': json_object.get('age', "")
        }
        self.storage.append(new_user)

        return make_response(jsonify(new_user), 201)

    def delete_user(self, id):
        u = [user for user in self.storage if user['id'] == id]
        if len(u) == 0:
            abort(404)
        self.storage.remove(u[0])
        return make_response(jsonify({'Succes Completed': 'Element Deleted'}), 201)

    def get_group(self):
        return jsonify(self.group_storage)

    def create_Group(self, json_object):
        u = [user for user in self.storage if user['name'] == json_object['name']]
        if len(u) != 0:
            abort(400)
        new_grupo = {
            'self': 'rest/group/' + str(json_object['name']),
            'name': json_object['name']
        }
        self.group_storage.append(new_grupo)
        return make_response(jsonify(new_grupo), 201)