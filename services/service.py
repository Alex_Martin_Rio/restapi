

class Server:

    def __init__(self, database):
        self.db = database

    def get_users(self):
        users = self.db.get_users()
        return users

    def get_user_Byid(self, id):
        user = self.db.get_user_ById(id)
        return user

    def get_User_DataByID(self, id ,data):
        info = self.db.get_User_DataByID(id, data)
        return info

    def updatae_user_DataById(self, id, data, json_object):
        response = self.db.updatae_user_DataById(id, data, json_object)
        return response

    def delete_user_DatabyID(self, id, data):
        response = self.db.delete_user_DatabyID(id, data)
        return response

    def add_user(self, json_object):
        user_added = self.db.add_user(json_object)
        return user_added

    def delete_user(self, id):
        user_deleted = self.db.delete_user(id)
        return user_deleted

    def get_group(self):
        groups = self.db.get_group()
        return groups

    def create_Group(self, json_object):
        grupos = self.db.create_Group(json_object)
        return grupos